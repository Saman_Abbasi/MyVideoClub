﻿using MyVideoClub.Models.VideoClubDBModels;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyVideoClub.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Register()
        {
            var model = new MemberUserModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult Register(MemberUserModel model)
        {
            if (ModelState.IsValid)
            {
                var getJson = "http://localhost:49370/api/MemberUser";
                SimplePost<MemberUserModel>(model, getJson);
                ViewBag.create = " You are registered ... !";
                return View();
            }
            return View(model);
        }
        [HttpGet]
        public ActionResult AddMovie()
        {
            var model = new Video();
            return View(model);
        }
        [HttpPost]
        public ActionResult AddMovie(Video model)
        {
            if (ModelState.IsValid)
            {
                var getJson = "http://localhost:49370/api/Video";
                SimplePost<Video>(model, getJson);
                ViewBag.create = " You Film was registered ... !";
                return View();
            }
            return View(model);
        }
        [HttpGet]
        public ActionResult EditMovie(Guid id)
        {
            var url = "http://localhost:49370/api/";
            var model = SimpleGetId(url, id, "Video");
            var result = JsonConvert.DeserializeObject<Video>(model.Content);
            return View(result);
        }
        [HttpPost]
        public ActionResult EditMovie(Video model)
        {
            var id = model.MovieId;
            var url = "http://localhost:49370/api/";
            if (model.MovieCount == 0)
            {
                model.MovieCount = 1;
            }
            var put = SimplePut<Video>(id, model, url, "Video");
            return RedirectToAction("SeeAllMovie");
        }
        [HttpGet]
        public ActionResult EditMember(Guid id)
        {
            var url = "http://localhost:49370/api/";
            var modelJason = SimpleGetId(url, id, "memberuser");
            var model = JsonConvert.DeserializeObject<MemberUserModel>(modelJason.Content);
            return View(model);
        }
        [HttpPost]
        public ActionResult EditMember(MemberUserModel value)
        {
            var url = "http://localhost:49370/api/";
            var model = SimplePut<MemberUserModel>(value.MemberUserId, value, url, "memberuser");
            return RedirectToAction("SeeAllMember");
        }
        public ActionResult SeeAllMember()
        {
            var url = "http://localhost:49370/api/MemberUser";
            var model = SimpleGetAsync(url);
            var result = JsonConvert.DeserializeObject<MemberUserModel[]>(model.Content);
            return View(result);
        }
        public ActionResult SeeAllMovie()
        {
            var url = "http://localhost:49370/api/Video";
            var modelJason = SimpleGetAsync(url);
            var result = JsonConvert.DeserializeObject<Video[]>(modelJason.Content);
            return View(result);
        }
        [HttpGet]
        public ActionResult SearchMember()
        {
            return PartialView();
        }
        [HttpPost]
        public ActionResult SearchMember(string email)
        {
            var url = "http://localhost:49370/api/MemberUser";
            var model = SimpleGetAsync(url);
            var result = JsonConvert.DeserializeObject<MemberUserModel[]>(model.Content);
            var user = result.FirstOrDefault(a => a.Email == email);
            return PartialView(user);
        }
        [HttpGet]
        public ActionResult SearchMovie()
        {
            return PartialView();
        }
        [HttpPost]
        public ActionResult SearchMovie(string movieName)
        {
            var url = "http://localhost:49370/api/Video";
            var model = SimpleGetAsync(url);
            var result = JsonConvert.DeserializeObject<Video[]>(model.Content);
            var user = result.FirstOrDefault(a => a.MovieName == movieName);
            return PartialView(user);
        }
        [HttpGet]
        public ActionResult RentMovie()
        {
            var memberUrl = "http://localhost:49370/api/MemberUser";
            var movieUrl = "http://localhost:49370/api/Video";
            var movieModel = SimpleGetAsync(movieUrl);
            var memberModel = SimpleGetAsync(memberUrl);
            var movie = JsonConvert.DeserializeObject<Rent[]>(movieModel.Content);
            var member = JsonConvert.DeserializeObject<Rent[]>(memberModel.Content);
            //*************************************************
            List<Rent> rentList = new List<Rent>();
            List<Rent> movieList = new List<Rent>(movie);
            List<Rent> memberList = new List<Rent>(member);
            foreach (var item in memberList)
            {
                var model = new Rent()
                {
                    Name = item.Name,
                    Lastname = item.Lastname,
                    Age = item.Age,
                    Email = item.Email,
                    MemberUserId = item.MemberUserId
                };
                rentList.Add(model);
            }

            foreach (var item2 in movieList)
            {
                var model = new Rent()
                {
                    MovieName = item2.MovieName,
                    MovieCount = item2.MovieCount,
                    MovieId = item2.MovieId,
                    Topic = item2.Topic,
                    Description = item2.Description
                };
                rentList.Add(model);
            }
            return View(rentList);
            //*************************************************
            // code zir ham doroste balaii jahate tamrine kar ba list ha bod
            //rentList.AddRange(movie);
            //rentList.AddRange(member);
            //return View(rentList);
        }
        [HttpPost]
        public ActionResult RentMovie(Rent model)//Guid memberId , Guid movieId)
        {
           //// var url = $"http://localhost:49370/api/rent?memberId={memberId}&movieId={movieId}";
            var url = "http://localhost:49370/api/rent";
            var result = PostRent(url,model);
            return View();
        }
        public ActionResult Login()
        {
            return View();
        }

        // ___________________________________________________________
        public JsonResult EmailExists(string email)
        {
            var getJson = SimpleGetAsync("http://localhost:49370/api/MemberUser");
            var model = JsonConvert.DeserializeObject<MemberUserModel[]>(getJson.Content);
            var result = model.Any(a => a.Email == email);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult MovieNameExists(string moviename)
        {
            var getJson = SimpleGetAsync("http://localhost:49370/api/Video");
            var model = JsonConvert.DeserializeObject<Video[]>(getJson.Content);
            var result = model.Any(a => a.MovieName == moviename);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        private IRestResponse SimpleGetAsync(string url)
        {
            var client = new RestClient(url);
            var restRq = new RestRequest(Method.GET);
            var response = client.Execute(restRq);
            return response;
        }
        public IRestResponse SimpleGetId(string url, Guid id, string controllername)
        {
            var client = new RestClient(url);
            var request = new RestRequest(controllername + "/" + id, Method.GET);
            var response = client.Execute(request);
            return response;
        }
        protected static IRestResponse SimplePost<T>(object objectToUpdate, string url) where T : new()
        {

            var client = new RestClient(url);
            var restRq = new RestRequest()
            {
                Method = Method.POST,
                RootElement = "/",
                RequestFormat = DataFormat.Json
            };
            var json = JsonConvert.SerializeObject(objectToUpdate);
            restRq.AddHeader("Authorization", "Basic ");// + authKey);
            restRq.AddParameter("text/json", json, ParameterType.RequestBody);

            var response = client.Execute<T>(restRq);
            return response;
        }
        protected static IRestResponse PostRent(string url,object model)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.POST);
            request.AddJsonBody(model);
            var result = client.Execute(request);
            return result;
        }
        protected static IRestResponse SimplePut<T>(Guid id, object objectToUpdate, string url, string controllerName) where T : new()
        {
            //var client = new RestClient(url);
            //var restRq = new RestRequest()
            //{
            //    Method = Method.PUT,
            //    RootElement = "/",
            //    RequestFormat = DataFormat.Json
            //};
            //var json = JsonConvert.SerializeObject(objectToUpdate);
            //restRq.AddHeader("Authorization", "Basic ");// + authKey);
            //restRq.AddParameter("text/json", json, ParameterType.RequestBody);
            //restRq.AddParameter("id", id);
            //var response = client.Execute<T>(restRq);
            //var client = new RestClient(url);
            //var client = new RestClient("http://localhost:49370/api/");
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            var client = new RestClient(url);
            var request = new RestRequest(controllerName + "/" + id, Method.PUT);
            request.AddJsonBody(objectToUpdate);
            var response = client.Execute(request);
            return response;
        }
    }
}