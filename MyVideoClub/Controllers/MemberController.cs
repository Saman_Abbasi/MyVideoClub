﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using RestSharp;
using MyVideoClub.Models.VideoClubDBModels;

namespace MyVideoClub.Controllers
{
    public class MemberController : Controller
    {
        // GET: Member
        public ActionResult Index()
        {
            var getJson = SimpleGetAsync("http://localhost:49370/api/MemberUser");
            var model = JsonConvert.DeserializeObject<MemberUserModel[]>(getJson.Content);
            return View(model);
        }
        [HttpPost]
        public ActionResult Index(MemberUserModel model)
        {
            var getJson = "http://localhost:49370/api/MemberUser";
            SimplePost<MemberUserModel>(model, getJson);
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Put()

        {
            var model = new MemberUserModel() { Name = "Shahab", Lastname = "Jamal Abbasi" };
            Guid id = new Guid("836E504C-6E8A-42A8-B69F-03582095A6F1");
            var url = "http://localhost:49370/api/";
            SimplePut<MemberUserModel>(id, model, url);
            return View();
        }
        public ActionResult Delete()
        {
            Guid id = new Guid("836E504C-6E8A-42A8-B69F-03582095A6F1");
            var url = "http://localhost:49370/api/";
            SimpleDelete(id, url);
            return RedirectToAction("Index");
        }
        public ActionResult Get()
        {
            Guid id = new Guid("836E504C-6E8A-42A8-B69F-03582095A6F0");
            var url = "http://localhost:49370/api/";
            var model = SimpleGetId(url, id);
            var json = JsonConvert.DeserializeObject<MemberUserModel>(model.Content);
            return View(json);
        }
        public IRestResponse SimpleGetAsync(string url)
        {
            var client = new RestClient(url);
            var restRq = new RestRequest()
            {
                Method = Method.GET,
                RootElement = "/",
                RequestFormat = DataFormat.Json
            };
            var restRs = client.Execute(restRq);
            return restRs;
        }
        public IRestResponse SimpleGetId(string url,Guid id)
        {
            var client = new RestClient(url);
            var request = new RestRequest("MemberUser/" + id, Method.GET);
            var response = client.Execute(request);
            return response;
        }
        protected static IRestResponse SimplePost<T>(object objectToUpdate, string url) where T : new()
        {

            var client = new RestClient(url);
            var restRq = new RestRequest()
            {
                Method = Method.POST,
                RootElement = "/",
                RequestFormat = DataFormat.Json
            };
            var json = JsonConvert.SerializeObject(objectToUpdate);
            restRq.AddHeader("Authorization", "Basic ");// + authKey);
            restRq.AddParameter("text/json", json, ParameterType.RequestBody);

            var response = client.Execute<T>(restRq);
            return response;
        }
        protected static IRestResponse SimplePut<T>(Guid id, object objectToUpdate, string url) where T : new()
        {
            //var client = new RestClient(url);
            //var restRq = new RestRequest()
            //{
            //    Method = Method.PUT,
            //    RootElement = "/",
            //    RequestFormat = DataFormat.Json
            //};
            //var json = JsonConvert.SerializeObject(objectToUpdate);
            //restRq.AddHeader("Authorization", "Basic ");// + authKey);
            //restRq.AddParameter("text/json", json, ParameterType.RequestBody);
            //restRq.AddParameter("id", id);
            //var response = client.Execute<T>(restRq);
            //var client = new RestClient(url);
            //var client = new RestClient("http://localhost:49370/api/");
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            var client = new RestClient(url);
            var request = new RestRequest("MemberUser/" + id, Method.PUT);
            request.AddJsonBody(objectToUpdate);
            var response = client.Execute(request);
            return response;
        }
        protected static IRestResponse SimpleDelete(Guid id,string url)
        {
            var client = new RestClient(url);
            var request = new RestRequest("MemberUser/" + id, Method.DELETE);
            var response = client.Execute(request);
            return response;
        }
    }
}
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
//var client = new RestClient();
////var client = new RestClient("");


//var restRq = new RestRequest(url)
//{
//    Method = Method.POST,
//    RootElement = "/",
//    RequestFormat = DataFormat.Json
//};
//var json = JsonConvert.SerializeObject(objectToUpdate);
//restRq.AddHeader("Authorization", "Basic " + authKey);
//       restRq.AddParameter("text/json", json, ParameterType.RequestBody);

//        var response = client.Execute < T😠restRq);


//        return response;
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
//    using RestSharp;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.Mvc;
//using Newtonsoft.Json;
//namespace WebApplication1.Controllers
//{
//    public class HomeController : Controller
//    {
//        public ActionResult Index()
//        {
//            var getJson = SimpleGetAsync("http://localhost:23431/api/saman");
//            var model = JsonConvert.DeserializeObject < Venue[]😠getJson.Content);
//            return View();
//        }
//        public IRestResponse SimpleGetAsync(string url)
//        {
//            var client = new RestClient(url);
//            var restRq = new RestRequest()
//            {
//                Method = Method.GET,
//                RootElement = "/",
//                RequestFormat = DataFormat.Json
//            };
//            var restRs = client.Execute(restRq);
//            return restRs;
//        }
//        protected static IRestResponse SimplePost<T😠object objectToUpdate, string url) where T : new()
//        {

//            var client = new RestClient(url);
//            //var client = new RestClient("");


//            var restRq = new RestRequest()
//            {
//                Method = Method.POST,
//                RootElement = "/",
//                RequestFormat = DataFormat.Json
//            };
//            var json = JsonConvert.SerializeObject(objectToUpdate);
//            var response = client.Execute < T😠restRq);
//            return response;
//        }
//       }
//}