﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyVideoClub.Models.VideoClubDBModels
{
    public class MemberUserModel
    {
        public Guid MemberUserId { get; set; }
        [Required(ErrorMessage = "وارد کردن نام اجباری می باشد")]
        [Display(Name = "نام :  ")]
        [StringLength(40, MinimumLength = 3, ErrorMessage = "طول کارکتر وارد شده مجاز نمی باشد ")]
        public string Name { get; set; }
        [Required(ErrorMessage = "وارد کردن نام اجباری می باشد")]
        [Display(Name = " نام خانوادگی :  ")]
        [StringLength(40, MinimumLength = 4, ErrorMessage = "طول کارکتر وارد شده مجاز نمی باشد ")]
        public string Lastname { get; set; }
        //[Required(ErrorMessage = "وارد کردن نام اجباری می باشد")]
        [Display(Name = "سن :  ")]
        //[StringLength(40, MinimumLength = 4, ErrorMessage = "طول کارکتر وارد شده مجاز نمی باشد ")]
        public int? Age { get; set; }
        [Required(ErrorMessage = "وارد کردن نام اجباری می باشد")]
        [Display(Name = "ایمیل  :  ")]
        [StringLength(40, MinimumLength = 4, ErrorMessage = "طول کارکتر وارد شده مجاز نمی باشد ")]
        public string Email { get; set; }
        [Required(ErrorMessage = "وارد کردن نام اجباری می باشد")]
        [Display(Name = " پسورد :  ")]
        [StringLength(40, MinimumLength = 4, ErrorMessage = "طول کارکتر وارد شده مجاز نمی باشد ")]
        public string PasswordHash { get; set; }
        [Display(Name = " تاریخ عضویت :  ")]
        public DateTime RegisterDate { get; set; }

    }
}