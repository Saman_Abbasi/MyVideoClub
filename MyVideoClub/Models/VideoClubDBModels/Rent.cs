﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyVideoClub.Models.VideoClubDBModels
{
    public class Rent
    {
        public Guid MovieId { get; set; }
        public Guid MemberUserId { get; set; }
        public string Name { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public int Age { get; set; }
        public string MovieName { get; set; }
        public string Topic { get; set; }
        public string Description { get; set; }
        public int MovieCount { get; set; }
    }
}