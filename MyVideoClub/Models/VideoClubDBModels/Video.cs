﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyVideoClub.Models.VideoClubDBModels
{
    public class Video
    {
        public Guid MovieId { get; set; }
        [Required(ErrorMessage = "وارد کردن نام فیلم اجباری می باشد")]
        [Display(Name = "فیلم نام :  ")]
        [StringLength(40, MinimumLength = 4, ErrorMessage = "طول کارکتر وارد شده مجاز نمی باشد ")]
        public string MovieName { get; set; }
        [Required(ErrorMessage = "وارد کردن توضیحات اجباری می باشد")]
        [Display(Name = "توضیحات  :  ")]
        [StringLength(40, MinimumLength = 4, ErrorMessage = "طول کارکتر وارد شده مجاز نمی باشد ")]
        public string Description { get; set; }
        [Required(ErrorMessage = "وارد کردن عنوان فیلم اجباری می باشد")]
        [Display(Name = "عنوان فیلم :  ")]
        [StringLength(40, MinimumLength = 4, ErrorMessage = "طول کارکتر وارد شده مجاز نمی باشد ")]
        public string Topic { get; set; }
        [Required(ErrorMessage = "وارد کردن تعداد فیلم اجباری می باشد")]
        [Display(Name = "تعداد فیلم :  ")]
        //[StringLength(40, MinimumLength = 4, ErrorMessage = "طول کارکتر وارد شده مجاز نمی باشد ")]
        public int MovieCount { get; set; }
    }
}